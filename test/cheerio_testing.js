#!/usr/bin/env node
// cSpell:disable
const cheerio = require("cheerio");
const fetch = require("node-fetch");

fetch(
    "https://www.swimmingrank.com/ii/strokes/strokes_il/NGIQKAMFA_200IM.html"
).then((response) => {
    response.text().then((text) => {
        let $ = cheerio.load(text);
        let yd_time = $(
            "#content > table:nth-child(6) > tbody:nth-child(1) > tr:nth-child(2) > td:nth-child(2)"
        ).text();
        console.log("yd_time", yd_time);
        let m_time = $(
            "#content > table:nth-child(6) > tbody:nth-child(1) > tr:nth-child(3) > td:nth-child(2)"
        ).text();
        console.log("m_time", m_time);
    });
});
