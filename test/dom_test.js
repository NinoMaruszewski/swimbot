#!/usr/bin/env node
const fetch = require("node-fetch");
const swim_tools = require("../functions/swim_tools");

swim_tools
    .getHTMLDom(
        "https://www.swimmingrank.com/ii/strokes/strokes_il/NGIQKAMFA_200IM.html"
    )
    .then((responseDOM) => {
        // extract elements
        let elements = responseDOM.window.document.querySelectorAll("td");

        // HACK this only works if the website stays as a static html. Kinda slow.
        // check if the elements are correct
        let ydTime = elements[6].textContent.match("[0-9]*:*[0-9]*\\.[0-9]*");
        console.log("ydTime", ydTime);
        let mTime = elements[11].textContent.match("[0-9]*:*[0-9]*\\.[0-9]*");
        console.log("mTime", mTime);
    });
