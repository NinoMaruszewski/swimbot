#!/usr/bin/env bash
# -*- coding: utf-8 -*-
for file in $@; do
    echo $file:
    time node $file
    echo
    echo
done
