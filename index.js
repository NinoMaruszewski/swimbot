// ===========================
// require all needed modules
// ===========================
const Discord = require("discord.js");
const fs = require("fs");
const { prefix, token } = require("./data/config.json");
const tools = require("./functions/tools.js");

// create a new Discord client
const client = new Discord.Client();

// =====================
// import command files
// =====================
// info commands
client.commands = new Discord.Collection();

const infoCommandFiles = fs
    .readdirSync("./commands/info")
    .filter((file) => file.endsWith(".js"));

for (const file of infoCommandFiles) {
    const command = require(`./commands/info/${file}`);

    // set a new item in the Collection
    // with the key as the command name and the value as the exported module
    client.commands.set(command.name, command);
}

// repeat for mod commands
const modCommandFiles = fs
    .readdirSync("./commands/moderation")
    .filter((file) => file.endsWith(".js"));

for (const file of modCommandFiles) {
    const command = require(`./commands/moderation/${file}`);

    // set a new item in the Collection
    // with the key as the command name and the value as the exported module
    client.commands.set(command.name, command);
}

// ===============
// cooldowns setup
// ===============
const cooldowns = new Discord.Collection();

// ========================================================
// when the client is ready, run this code
// this event will only trigger one time after logging in
// ========================================================
client.once("ready", () => {
    client.user.setPresence({
        activity: {
            name: "$help",
            type: "WATCHING",
        },
        status: "online",
    });
    console.log("Ready!");
});

// ===================
//  message responder
// ===================
client.on("message", (message) => {
    // make sure this is for the bot
    if (!message.content.startsWith(prefix) || message.author.bot) return;

    // get arguments
    const args = message.content.slice(prefix.length).split(/ +/);
    const commandName = args.shift().toLowerCase();

    // ==============================
    // aliases/valid command checking
    // ==============================
    const command =
        client.commands.get(commandName) ||
        client.commands.find(
            (cmd) => cmd.aliases && cmd.aliases.includes(commandName)
        );

    if (!command) return;

    // =========================
    // Guild only stuff checking
    // =========================
    if (command.guildOnly && message.channel.type === "dm") {
        return message.reply("I can't execute that command inside DMs!");
    }

    // ===============
    //  args checking
    // ===============
    // make sure the command has arguments that it needs, if it doesn't have an exact amount
    if (command.args && command.minArgs && command.minArgs > args.length) {
        let reply = `You didn't provide the correct number of arguments, ${message.author}! \nYou need to provide at least ${command.minArgs} argument(s).`;

        // check if usage is specified
        if (command.usage) {
            reply += `\nThe proper usage would be: \`${prefix}${command.name} ${command.usage}\``;
        }

        return message.channel.send(reply);
    }
    // then check if it has an exact amount
    else if (
        command.args &&
        args.length !== command.numArgs &&
        command.numArgs !== null
    ) {
        let reply = `You didn't provide the correct number of arguments, ${message.author}!`;

        // check if usage is specified
        if (command.usage) {
            reply += `\nThe proper usage would be: \`${prefix}${command.name} ${command.usage}\``;
        }

        return message.channel.send(reply);
    }

    // =========
    // cooldowns
    // =========

    // check if command is in cooldowns collection. if not, add it
    if (!cooldowns.has(command.name)) {
        cooldowns.set(command.name, new Discord.Collection());
    }

    // get variables for keeping track of timestamps
    const now = Date.now();
    const timestamps = cooldowns.get(command.name);
    const cooldownAmount = (command.cooldown || 1) * 1000;

    // check if cooldown has expired
    if (timestamps.has(message.author.id)) {
        const expirationTime =
            timestamps.get(message.author.id) + cooldownAmount;

        if (now < expirationTime) {
            const timeLeft = (expirationTime - now) / 1000;
            return message.reply(
                `please wait ${timeLeft.toFixed(
                    1
                )} more second(s) before reusing the \`${
                    command.name
                }\` command.`
            );
        }
    }

    // the the user to the timestamps, if it hasn't expired, and then after the timeout, delete them
    timestamps.set(message.author.id, now);
    setTimeout(() => timestamps.delete(message.author.id), cooldownAmount);

    // ========================
    // check if user has perms
    // ========================
    if (command.mod && !message.member.hasPermission("MANAGE_MESSAGES")) {
        return message.reply(
            "you do not have the permissions to run that command."
        );
    }

    // ===================
    // execute the command
    // ===================
    try {
        command.execute(message, args);
    } catch (error) {
        if (
            error.message === "InputSpellingError" ||
            error.message === "NotSwamEvent"
        ) {
            // do nothing
        } else {
            console.error(error);
            message.reply("there was an error trying to execute that command!");
        }
    }
});

// ===========
//  auto-role
// ===========
client.on("guildMemberAdd", (member) => {
    let dj_role = tools.get_role_id("dj", member);
    if (!dj_role) {
        console.log(`No DJ role on ${member.guild.name}, not adding.`);
    } else {
        member.roles.add(dj_role);
    }
});

// login to Discord with your app's token
client.login(token);
