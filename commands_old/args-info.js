module.exports = {
	name: 'args-info',
    description: 'Describes arguments passed',
    args: true,
    numArgs: 4,
    usage: '<arg> <arg> <arg> <arg>',
	execute(message, args) {
        if (args[0] === 'foo') {
            return message.channel.send('bar');
        }

        message.channel.send(`Arguments: ${args}\nArguments length: ${args.length}`);
	},
};