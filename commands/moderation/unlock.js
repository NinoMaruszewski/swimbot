module.exports = {
    name: "unlock",
    description: "Unlock a channel. MOD ONLY.",
    cooldown: 0,
    args: false,
    numArgs: 0,
    usage: "",
    aliases: ['ul'],
    mod: true,
    guildOnly: true,
    execute(message, args) {
        let channel = message.channel;
        let everyone = message.guild.roles.everyone;
        channel
            .updateOverwrite(everyone, {
                SEND_MESSAGES: true,
            })
            .catch(console.error);
        channel.send("Channel unlocked.");
    },
};
