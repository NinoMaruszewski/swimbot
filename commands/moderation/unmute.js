// modules
const tools = require("../../functions/tools");

module.exports = {
    name: "unmute",
    description: "Unmute a user from sending messages. MOD ONLY.",
    cooldown: 0,
    args: true,
    numArgs: 1,
    usage: "<@user>",
    mod: true,
    guildOnly: true,
    aliases: ['um'],
    execute(message, args) {
        let user_to_unmute = message.mentions.members.first();
        if (!user_to_unmute) {
            return message.reply("please mention a valid user.");
        } else {
            let mute_role = tools.get_role_id("muted", message);
            user_to_unmute.roles.remove(mute_role);
            message.channel.send(`Unmuted ${user_to_unmute}`);
        }
    },
};
