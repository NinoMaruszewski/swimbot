module.exports = {
    name: "lock",
    description: "Lock a channel. MOD ONLY.",
    cooldown: 0,
    args: false,
    numArgs: 0,
    usage: "",
    aliases: ['l'],
    mod: true,
    guildOnly: true,
    execute(message, args) {
        let channel = message.channel;
        let everyone = message.guild.roles.everyone;
        channel
            .updateOverwrite(everyone, {
                SEND_MESSAGES: false,
            })
            .catch(console.error);
        channel.send("Channel locked.");
    },
};
