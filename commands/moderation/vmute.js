module.exports = {
    name: "vmute",
    description: "Mute a user from talking. MOD ONLY.",
    cooldown: 0,
    args: true,
    numArgs: 1,
    usage: "<@user>",
    mod: true,
    guildOnly: true,
    aliases: ['vm'],
    execute(message, args) {
        let user_to_vmute = message.mentions.members.first();
        if (!user_to_vmute) {
            return message.reply("please mention a valid user.");
        } else if (!user_to_vmute.voice.channel) {
            return message.reply(
                "the user must be connected to a voice channel to voice mute/unmute them."
            );
        } else {
            user_to_vmute.voice.setMute(true);
        }
        message.channel.send(`Voice muted ${user_to_vmute}`);
    },
};
