// modules
const tools = require("../../functions/tools");

module.exports = {
    name: "mute",
    description: "Mute a user from sending messages. MOD ONLY.",
    cooldown: 0,
    args: true,
    numArgs: 1,
    usage: "<@user>",
    mod: true,
    aliases: ['m'],
    guildOnly: true,
    execute(message, args) {
        let user_to_mute = message.mentions.members.first();
        if (!user_to_mute) {
            return message.reply("please mention a valid user.");
        } else {
            let mute_role = tools.get_role_id("muted", message);
            user_to_mute.roles.add(mute_role);
            message.channel.send(`Muted ${user_to_mute}`);
        }
    },
};
