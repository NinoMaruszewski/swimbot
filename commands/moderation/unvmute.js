module.exports = {
    name: "unvmute",
    description: "Unmute a user from talking. MOD ONLY.",
    cooldown: 0,
    args: true,
    numArgs: 1,
    usage: "<@user>",
    mod: true,
    guildOnly: true,
    aliases: ['uvm'],
    execute(message, args) {
        let user_to_unvmute = message.mentions.members.first();
        if (!user_to_unvmute) {
            return message.reply("please mention a valid user.");
        } else if (!user_to_unvmute.voice.channel) {
            return message.reply(
                "the user must be connected to a voice channel to voice mute/unmute them."
            );
        } else {
            user_to_unvmute.voice.setMute(false);
        }
        message.channel.send(`Voice unmuted ${user_to_unvmute}`);
    },
};
