module.exports = {
    name: "clear",
    description: "Clear 1 - 99 messages from a channel. MOD ONLY.",
    cooldown: 0,
    args: true,
    numArgs: 1,
    usage: "<No. messages>",
    aliases: ['c'],
    mod: true,
    execute(message, args) {
        let no_messages = parseInt(args[0]) + 1;

        // valid no check
        if (isNaN(no_messages)) {
            return message.reply("that doesn't seem to be a valid number.");
        } else if (no_messages < 1 || no_messages > 100) {
            return message.reply(
                "you need to input a number between 2 and 100."
            );
        } else {
            // delete messages
            message.channel.bulkDelete(no_messages).catch((err) => {
                console.error(err);
                message.channel.send(
                    "there was an error trying to prune messages in this channel!"
                );
            });
        }
    },
};
