module.exports = {
    name: "beep",
    description: "I'm a robot!",
    cooldown: 1,
    args: false,
    numArgs: null,
    usage: "",
    mod: false,
    // eslint-disable-next-line no-unused-vars
    execute(message, args) {
        message.channel.send("Boop.");
    },
};
