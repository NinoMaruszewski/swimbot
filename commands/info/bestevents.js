// functions
const swim_tools = require("../../functions/swim_tools");
const tools = require("../../functions/tools.js");

module.exports = {
    name: "best-events",
    description: "Set your role to your best events",
    aliases: ['b-e', 'be', 'events'],
    cooldown: 60,
    args: true,
    numArgs: null,
    minArgs: 1,
    usage: "<butterfly> <backstroke> <breaststroke> <freestyle> <IM>",
    mod: false,
    guildOnly: true,
    execute(message, args) {
        // get role ids
        const fly_role = tools.get_role_id("butterfly", message);
        const back_role = tools.get_role_id("backstroke", message);
        const breast_role = tools.get_role_id("breaststroke", message);
        const free_role = tools.get_role_id("freestyle", message);
        const im_role = tools.get_role_id("^(im)[a-z]*", message);

        // add roles if needed
        const roles = [fly_role, back_role, breast_role, free_role, im_role];
        swim_tools.check_event_roles(roles, message.guild);

        // pull events
        let item;
        let events = [];
        for (item of args) {
            try {
                // eslint-disable-next-line comma-dangle
                let [min_ev] = swim_tools.parse_style(item);
                events.push(min_ev);
            } catch (e) {
                if (e.message === "InputSpellingError") {
                    message.reply(
                        "please check the style that you have entered."
                    );
                }
                throw e;
            }
        }

        // clean the array
        let events_clean = Array.from(new Set(events));

        // remove all roles
        message.member.roles.remove(roles);
        // search and add roles
        let event;
        for (event of events_clean) {
            switch (event) {
                case "fl":
                    message.member.roles.add(fly_role).catch(console.error);
                    break;
                case "bk":
                    message.member.roles.add(back_role).catch(console.error);
                    break;
                case "br":
                    message.member.roles.add(breast_role).catch(console.error);
                    break;
                case "fr":
                    message.member.roles.add(free_role).catch(console.error);
                    break;
                case "im":
                    message.member.roles.add(im_role).catch(console.error);
                    break;
                default:
                    console.log("Something went wrong with the switch.");
                    throw new Error("SwitchError");
            }
        }

        // let user know
        message.reply("your roles have been updated.");
    },
};
