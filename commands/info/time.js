// libs
const fs = require("fs");
const cheerio = require('cheerio')

// personal function files
const embeds = require("../../functions/embeds.js");
const tools = require("../../functions/tools.js");
const swim_tools = require("../../functions/swim_tools.js");

module.exports = {
    name: "time",
    description: "Look up and print out the time of a swimmer in an event.",
    aliases: ['t'],
    cooldown: 10,
    args: true,
    numArgs: 4,
    mod: false,
    usage:
        "<first_name> <last_name> <distance> <butterfly (fl)/backstroke (bk)/breaststroke (br)/freestyle (fr)/IM>",
    execute(message, args) {
        // start typing
        message.channel.startTyping();
        // assign vars
        const name_UUIDs_path = "./data/name_uuids.json";
        var args_lower = tools.lowercase_list(args);
        const firstName = args_lower[0];
        const lastName = args_lower[1];
        const distance = args_lower[2];
        let style = "";
        let style_nice = "";
        var swimRankURL = "";
        // filter by style for more spelling options
        try {
            [style, style_nice] = swim_tools.parse_style(args_lower[3]);
        } catch (e) {
            if (e.message === "InputSpellingError") {
                message.reply("please check the style that you have entered.");
            }
            throw e;
        }
        // console.log(args_lower);

        // open storage files
        let uuids = JSON.parse(fs.readFileSync(name_UUIDs_path, "utf8"));

        // pull the uuid
        swim_tools
            .check_JSON_for_swimmer(
                uuids,
                firstName,
                lastName,
                message,
                name_UUIDs_path
            )
            .then((response) => {
                // make the URL
                const swimmer_uuid = response[lastName][firstName].uuid;
                const swimmer_zone = response[lastName][firstName].zone;
                const swimmer_lsc = response[lastName][firstName].lsc;

                try {
                    swimRankURL = swim_tools.swimrank_URL_gen(
                        swimmer_zone,
                        swimmer_lsc,
                        swimmer_uuid,
                        distance,
                        style
                    );
                } catch (e) {
                    if (e.message === "InputSpellingError") {
                        message.reply(
                            "please check the distance that you have entered."
                        );
                    }
                    throw e;
                }
                // console.log(swimRankURL);

                // get html in dom
                swim_tools
                    .get_HTML(swimRankURL)
                    .then((responseHTML) => {
                        // set up cheerio
                        let $ = cheerio.load(responseHTML);

                        // pull elements
                        let ydTime = $("#content > table:nth-child(6) > tbody:nth-child(1) > tr:nth-child(2) > td:nth-child(2)").text()
                        let mTime = $("#content > table:nth-child(6) > tbody:nth-child(1) > tr:nth-child(3) > td:nth-child(2)").text()
                        let course = $('#content > table:nth-child(6) > tbody:nth-child(1) > tr:nth-child(2) > td:nth-child(1)').text()

                        let embedYTime, embedMTime;
                        if (ydTime !== null && mTime !== '') {
                            embedYTime = ydTime;
                            embedMTime = mTime;
                        } else if (ydTime !== null && mTime === '') {
                            if (course.match["yd"] !== null) {
                                embedYTime = ydTime;
                                embedMTime = "N/A";
                            } else {
                                embedYTime = "N/A";
                                embedMTime = ydTime;
                            }
                        } else {
                            message.reply(
                                "the swimmer you entered has not swam this event yet!"
                            );
                            throw new Error("NotSwamEvent");
                        }

                        let distanceMet, distanceYd;
                        // get right distance
                        if (
                            distance === "50" ||
                            distance === "100" ||
                            distance === "200" ||
                            (distance === "400" && style === "im")
                        ) {
                            distanceMet = distance;
                            distanceYd = distance;
                        } else if (
                            style === "fr" &&
                            (distance === "500" || distance === "400")
                        ) {
                            distanceMet = "400";
                            distanceYd = "500";
                        } else if (
                            style === "fr" &&
                            (distance === "800" || distance === "1000")
                        ) {
                            distanceMet = "800";
                            distanceYd = "1000";
                        } else if (
                            style === "fr" &&
                            (distance === "1500" ||
                                distance === "1650" ||
                                distance === "mile")
                        ) {
                            distanceMet = "1500";
                            distanceYd = "1650";
                        } else {
                            message.reply(
                                "please check the distance that you have entered."
                            );
                            throw new Error("InputSpellingError");
                        }

                        // stop typing
                        message.channel.stopTyping();

                        // send URL
                        message.channel.send(swimRankURL);

                        // send the embed
                        embeds.timeEmbed(
                            "#0356fc",
                            tools.capitalize(firstName) +
                                " " +
                                tools.capitalize(lastName),
                            swimRankURL,
                            "https://www.pinclipart.com/picdir/middle/92-929075_swimming-clipart-transparent-background-black-and-white-swimmer.png",
                            embedMTime,
                            embedYTime,
                            distanceMet,
                            distanceYd,
                            style_nice,
                            message
                        );
                    })
                    .catch((e) => {
                        throw e;
                    });
            })
            .catch((e) => {
                throw e;
            });
    },
};
