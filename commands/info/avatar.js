// functions
const avatar_embed = require("../../functions/embeds").avatarEmbed;

module.exports = {
    name: "avatar",
    description: "Find a user's avatar.",
    aliases: ["av", "pfp"],
    cooldown: 5,
    args: true,
    minArgs: 0,
    numArgs: null,
    mod: false,
    usage: "<@user>",
    guildOnly: false,
    execute(message, args) {
        // no one mentioned, so get sender avatar
        if (!args) {
            var avatar_list = [
                [
                    message.author.displayAvatarURL({
                        format: "png",
                        dynamic: true,
                    }),
                    message.author.username,
                ],
            ];
        }

        // 1+ people mentioned, so get their avatars individually
        else {
            var avatar_list = message.mentions.users.map((user) => {
                return [
                    user.displayAvatarURL({
                        format: "png",
                        dynamic: true,
                    }),
                    user.username,
                ];
            });
        }

        // send embeds
        avatar_list.forEach((avatar_user_list) => {
            avatar_embed(
                avatar_user_list[0],
                avatar_user_list[1],
                "#9003fc",
                message
            );
        });
    },
};
