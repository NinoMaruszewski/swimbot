// get prefix
const { prefix } = require("../../data/config.json");
const tools = require("../../functions/tools");

module.exports = {
    name: "help",
    description: "List all of my commands or info about a specific command.",
    usage: "<command name>",
    cooldown: 5,
    mod: false,
    aliases: ["h"],
    execute(message, args) {
        const data = [];
        const { commands } = message.client;

        // Print all commands if no args passed
        if (!args.length) {
            data.push("Here's a list of all my commands:`");
            data.push(commands.map((command) => command.name).join(", "));
            data.push(
                `\`\nYou can send \`${prefix}help [command name]\` to get info on a specific command!`
            );

            return message.channel.send(data, { split: true });
        }

        // Make sure, if args passed, that they are valid commands
        const name = args[0].toLowerCase();
        const command =
            commands.get(name) ||
            commands.find((c) => c.aliases && c.aliases.includes(name));

        if (!command) {
            return message.reply("that's not a valid command!");
        }

        // Get the command name
        data.push(`**Name:** ${command.name}`);

        // Get the command description, if available
        if (command.description) {
            data.push(`**Description:** ${command.description}`);
        }

        // Get the command usage, if available
        if (command.usage) {
            data.push(
                `**Usage:** \`${prefix}${command.name} ${command.usage}\``
            );
        }

        // Get the command aliases, if available
        if (command.aliases) {
            let message = `**Aliases**: `;
            command.aliases.forEach((alias) => {
                message += `\`${alias}\`, `;
            });
            data.push(message.slice(0, -2));
        }

        // Get the command cooldown, if available, otherwise set it to 1 second
        data.push(`**Cooldown:** ${command.cooldown || 1} second(s)`);

        // Print if the command is guild-only or not
        if (command.guildOnly) {
            data.push("**Guild Only:** True");
        } else {
            data.push("**Guild Only:** False");
        }

        // Send the message to the channel
        message.channel.send(data, { split: true });
    },
};
