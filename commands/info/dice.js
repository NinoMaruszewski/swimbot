// functions
const embeds = require("../../functions/embeds");
const tools = require("../../functions/tools");

module.exports = {
    name: "dice",
    description: "Roll some dice.",
    cooldown: 5,
    args: true,
    numArgs: 1,
    usage: "<number of dice to roll>",
    mod: false,
    execute(message, args) {
        const max_value = args * 6;
        const roll_result = tools.random_int(max_value);

        const die_url =
            "https://upload.wikimedia.org/wikipedia/commons/thumb/c/c4/2-Dice-Icon.svg/1200px-2-Dice-Icon.svg.png";

        embeds.diceEmbed(
            "#25e625",
            "Dice Roll Results",
            die_url,
            roll_result,
            message
        );
    },
};
