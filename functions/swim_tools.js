// cSpell:disable
// libs
const google = require("google-it");
const jsdom = require("jsdom");
const { JSDOM } = jsdom;
const fetch = require("node-fetch");
const fs = require("fs");

function parse_style(style_raw) {
    let style, style_nice;
    if (style_raw.startsWith("fl") || style_raw.startsWith("bu")) {
        style = "fl";
        style_nice = "Butterfly";
    } else if (style_raw.startsWith("ba") || style_raw === "bk") {
        style = "bk";
        style_nice = "Backstroke";
    } else if (style_raw.startsWith("br")) {
        style = "br";
        style_nice = "Breaststroke";
    } else if (style_raw.startsWith("fr")) {
        style = "fr";
        style_nice = "Freestyle";
    } else if (style_raw.startsWith("im")) {
        style = "im";
        style_nice = "IM";
    } else {
        throw new Error("InputSpellingError");
    }
    return [style, style_nice];
}

// HACK This works by googling the swimmer's name and seeing if their name is in the search results
async function getUUID(fName, lName, message) {
    let query = fName + " " + lName + " " + "site:swimmingrank.com";
    // console.log(query);

    let re = new RegExp(
        "https://www.swimmingrank.com/([a-z]+)/strokes/strokes_([a-z]{2})/([A-Z0-9]+)_.*.html"
    );

    let results = await google({
        limit: 1,
        "only-urls": "true",
        "no-display": "true",
        query: query,
    }).catch((e) => {
        console.log(e);
        message.reply("google is down!");
    });

    try {
        let info = await results[0]["link"].match(re);
        // console.log(info);
        let zone = info[1];
        let lsc = info[2];
        let uuid = info[3];

        return [uuid, zone, lsc];
    } catch (error) {
        console.error(error);
        message.reply("please check the spelling of the swimmer's name.");
        throw new Error("InputSpellingError");
    }
}

async function check_JSON_for_swimmer(array, fName, lName, message, json_path) {
    // update if they're not
    if (!array[lName]) {
        // pull uuid for interwebs
        const swimmer_info = await getUUID(fName, lName, message).catch((e) => {
            throw e;
        });
        const uuid = swimmer_info[0];
        const zone = swimmer_info[1];
        const lsc = swimmer_info[2];
        // console.log(uuid);

        // set first level of hierarchy
        array[lName] = {};

        // set second level
        array[lName][fName] = {
            uuid: uuid,
            zone: zone,
            lsc: lsc,
        };

        // save changes to the file
        fs.writeFile(json_path, JSON.stringify(await array, null, 4), (err) => {
            if (err) console.error(err);
        });
    } else if (!array[lName][fName]) {
        // pull uuid for interwebs
        const swimmer_info = await getUUID(fName, lName, message).catch((e) => {
            throw e;
        });
        const uuid = swimmer_info[0];
        const zone = swimmer_info[1];
        const lsc = swimmer_info[2];
        // console.log(uuid);

        // set second level, since first is already filled
        array[lName][fName] = {
            uuid: uuid,
            zone: zone,
            lsc: lsc,
        };

        // save changes to the file
        fs.writeFile(json_path, JSON.stringify(await array, null, 4), (err) => {
            if (err) console.error(err);
        });
    }

    return await array;
}

async function getHTMLDom(url) {
    const response = await fetch(url);
    const text = await response.text();
    const dom = new JSDOM(text);
    return dom;
}

async function get_HTML(url) {
    const response = await fetch(url);
    const text = await response.text();
    return text;
}

function swimrank_URL_gen(zone, lsc, uuid, distance, style) {
    let swim_rank_URL;
    if (
        distance === "50" ||
        distance === "100" ||
        distance === "200" ||
        (distance === "400" && style === "im")
    ) {
        swim_rank_URL =
            "https://www.swimmingrank.com/" +
            zone +
            "/strokes/strokes_" +
            lsc +
            "/" +
            uuid +
            "_" +
            distance +
            style.toUpperCase() +
            ".html";
    } else if (style === "fr" && (distance === "500" || distance === "400")) {
        swim_rank_URL =
            "https://www.swimmingrank.com/" +
            zone +
            "/strokes/strokes_" +
            lsc +
            "/" +
            uuid +
            "_" +
            "400500" +
            style.toUpperCase() +
            ".html";
    } else if (style === "fr" && (distance === "800" || distance === "1000")) {
        swim_rank_URL =
            "https://www.swimmingrank.com/" +
            zone +
            "/strokes/strokes_" +
            lsc +
            "/" +
            uuid +
            "_" +
            "8001000" +
            style.toUpperCase() +
            ".html";
    } else if (
        style === "fr" &&
        (distance === "1500" || distance === "1650" || distance === "mile")
    ) {
        swim_rank_URL =
            "https://www.swimmingrank.com/" +
            zone +
            "/strokes/strokes_" +
            lsc +
            "/" +
            uuid +
            "_" +
            "15001650" +
            style.toUpperCase() +
            ".html";
    } else {
        throw new Error("InputSpellingError");
    }
    return swim_rank_URL;
}

function check_event_roles(list_roles, guild) {
    if (!list_roles[0]) {
        // fly role
        guild.roles.create({
            data: {
                name: "Butterflyers",
                mentionable: true,
            },
            reason: "Swimbot roles",
        });
    }
    if (!list_roles[1]) {
        // back role
        guild.roles.create({
            data: {
                name: "Backstrokers",
                mentionable: true,
            },
            reason: "Swimbot roles",
        });
    }
    if (!list_roles[2]) {
        // breast role
        guild.roles.create({
            data: {
                name: "Breaststrokers",
                mentionable: true,
            },
            reason: "Swimbot roles",
        });
    }
    if (!list_roles[3]) {
        // breast role
        guild.roles.create({
            data: {
                name: "Freestylers",
                mentionable: true,
            },
            reason: "Swimbot roles",
        });
    }
    if (!list_roles[4]) {
        // breast role
        guild.roles.create({
            data: {
                name: "IMers",
                mentionable: true,
            },
            reason: "Swimbot roles",
        });
    }
}

module.exports = {
    parse_style,
    getUUID,
    check_JSON_for_swimmer,
    getHTMLDom,
    swimrank_URL_gen,
    check_event_roles,
    get_HTML,
};
