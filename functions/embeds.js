const Discord = require("discord.js");

module.exports.timeEmbed = function (
    color,
    title,
    url,
    thumbnailURL,
    meterTime,
    yardTime,
    lengthMeters,
    lengthYards,
    style,
    message
) {
    let embed = new Discord.MessageEmbed()
        .setColor(color)
        .setTitle(title)
        .setURL(url)
        .setThumbnail(thumbnailURL)
        .addFields(
            {
                name: lengthYards + " Yard " + style + ":",
                value: yardTime,
                inline: true,
            },
            {
                name: lengthMeters + " Meter " + style + ":",
                value: meterTime,
                inline: true,
            }
        );
    message.channel.send(embed);
};

module.exports.diceEmbed = function (
    color,
    title,
    thumbnailURL,
    number,
    message
) {
    let embed = new Discord.MessageEmbed()
        .setColor(color)
        .setTitle(title)
        .setURL(thumbnailURL)
        .addFields({ name: "Roll results", value: number, inline: false });
    message.channel.send(embed);
};

module.exports.avatarEmbed = function (avatarURL, user, color, message) {
    let embed = new Discord.MessageEmbed()
        .setColor(color)
        .setTitle(`${user}'s Avatar:`)
        .setURL(avatarURL)
        .setImage(avatarURL);
    message.channel.send(embed);
};
