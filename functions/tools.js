function capitalize(phrase) {
    return phrase
        .toLowerCase()
        .split(" ")
        .map((word) => word.charAt(0).toUpperCase() + word.slice(1))
        .join(" ");
};

function capitalize_list(list) {
    let item;
    let cap_list = [];
    for (item of list) {
        cap_list.push(capitalize(item));
    }
    return cap_list;
};

function lowercase_list(list) {
    let list_lower = [];
    for (var i = 0; i < list.length; i++) {
        list_lower.push(list[i].toLowerCase());
    }
    return list_lower;
};

function modify_role(message, role) {
    // check if already has role
    if (!message.member.roles.cache.has(role.id)) {
        // add if doesn't
        message.member.roles.add(role).catch(console.error);
    } else {
        // remove if does
        message.member.roles.remove(role).catch(console.error);
    }
};

function get_role_id(role_name_re, message) {
    return message.guild.roles.cache.find((role) =>
        role.name.toLowerCase().match(role_name_re)
    );
};

function random_int(max) {
    return Math.floor(Math.random() * Math.floor(max)) + 1;
}

module.exports = {
    capitalize,
    capitalize_list,
    lowercase_list,
    modify_role,
    get_role_id,
    random_int,
};
